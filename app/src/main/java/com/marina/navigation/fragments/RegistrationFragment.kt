package com.marina.navigation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.marina.navigation.R
import com.marina.navigation.ShowAndHideBottomNavBar
import com.marina.navigation.databinding.FragmentRegistrationBinding

class RegistrationFragment : Fragment(R.layout.fragment_registration) {

    private lateinit var binding: FragmentRegistrationBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentRegistrationBinding.bind(view)
        //скрываем BottomNavigationView
        (requireActivity() as ShowAndHideBottomNavBar).hideBottomView()

        binding.btnLoginToRegistration.setOnClickListener {
            findNavController().navigate(R.id.action_registrationFragment_to_loginFragment)
        }
    }

    companion object {
        fun newInstance() = RegistrationFragment()
    }
}
