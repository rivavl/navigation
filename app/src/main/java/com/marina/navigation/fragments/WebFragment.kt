package com.marina.navigation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.marina.navigation.R
import com.marina.navigation.ShowAndHideBottomNavBar
import com.marina.navigation.databinding.FragmentWebBinding

class WebFragment : Fragment(R.layout.fragment_web) {

    private lateinit var binding: FragmentWebBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentWebBinding.bind(view)
        //показываем BottomNavigationView
        (requireActivity() as ShowAndHideBottomNavBar).showBottomView()
    }

    companion object {
        fun newInstance() = WebFragment()
    }
}
